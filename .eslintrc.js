module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json',
        },
    plugins: ['@typescript-eslint'],
    extends: ['airbnb-typescript/base'],
    ignorePatterns: ['*.js'],
    rules: {
        '@typescript-eslint/no-explicit-any': 2,
    }
};