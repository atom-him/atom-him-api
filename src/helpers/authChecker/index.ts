import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { verify } from 'jsonwebtoken';
import { UserModel } from '@models';

export const APP_SECRET = 'Wnhp18h%UQhV4ekj5AAgZd#TB';

export async function authChecker(
  { context }: {context: ExpressContext},
  roles: string[],
): Promise<boolean> {
  const { authorization } = context.req.headers;
  if (authorization) {
    const token = authorization.replace('Bearer ', '');
    const { userId } = verify(token, APP_SECRET) as { userId: string };
    const user = await UserModel.findById(userId);
    return user !== null && roles.includes(user.role);
  }

  return false;
}
