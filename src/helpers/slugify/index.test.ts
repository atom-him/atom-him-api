import slugify from '.';

test('slugify', () => {
  expect(slugify('Классическая одежда')).toMatch('klassicheskaya-odezhda');
});
