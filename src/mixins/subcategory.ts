import { UserInputError, ForbiddenError } from 'apollo-server-express';

import { SubcategoryMixin } from '@types';

import { slugify } from '@helpers';
import { SubcategoryModel } from '@models';

const subcategoryMixin: SubcategoryMixin = {
  async getSubcategorySlug(title) {
    const slug = slugify(title);
    const subcategories = await SubcategoryModel.find();
    const slugError = subcategories.find((subcategory) => slug === subcategory.slug);
    if (slugError) {
      throw new ForbiddenError('The subcategory with such slug already exist');
    }
    return slug;
  },

  async getPopulatedSubcategory(id) {
    const subcategory = await this.subcategoryPopulate(SubcategoryModel.findById(id));

    if (!subcategory) {
      throw new UserInputError('Such subcategory not found');
    }
    return subcategory;
  },

  async getSubcategory(id) {
    const subcategory = await SubcategoryModel.findById(id);

    if (!subcategory) {
      throw new UserInputError('Such subcategory not found');
    }
    return subcategory;
  },

  subcategoryPopulate(subcategory) {
    return subcategory.populate('category').populate('products');
  },
};

export default subcategoryMixin;
