import { UserInputError, ForbiddenError } from 'apollo-server-express';

import { CategoryMixin } from '@types';

import { slugify } from '@helpers';
import { CategoryModel } from '@models';

const categoryMixin: CategoryMixin = {
  async getCategorySlug(title) {
    const slug = slugify(title);
    const categories = await CategoryModel.find();
    const slugError = categories.find((category) => slug === category.slug);
    if (slugError) {
      throw new ForbiddenError('The category with such slug already exist');
    }
    return slug;
  },

  async getCategory(id) {
    const category = await CategoryModel.findById(id);
    if (!category) {
      throw new UserInputError('Such category not found');
    }
    return category;
  },

  async getPopulatedCategory(id) {
    const category = await this.categoryPopulate(CategoryModel.findById(id));
    if (!category) {
      throw new UserInputError('Such category not found');
    }
    return category;
  },

  categoryPopulate(category) {
    return category.populate('subcategories').populate('products');
  },

  categoriesPopulate(categories) {
    return categories.populate('subcategories').populate('products');
  },
};

export default categoryMixin;
