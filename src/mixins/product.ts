import { DocumentType } from '@typegoose/typegoose';
import { UserInputError, ForbiddenError } from 'apollo-server-express';

import { ProductMixin, ProductClass } from '@types';

import { slugify } from '@helpers';
import { ProductModel } from '@models';

const productMixin: ProductMixin = {
  async getProductSlug(title) {
    const slug = slugify(title);
    const products = await ProductModel.find();
    const slugError = products.find((product) => slug === product.slug);
    if (slugError) {
      throw new ForbiddenError('The product with such slug already exist');
    }
    return slug;
  },

  async getPopulatedProduct(id): Promise<DocumentType<ProductClass>> {
    const product = await this.productPopulate(ProductModel.findById(id));
    if (!product) {
      throw new UserInputError('Such product not found');
    }
    return product;
  },

  async getProduct(id): Promise<DocumentType<ProductClass>> {
    const product = await ProductModel.findById(id);
    if (!product) {
      throw new UserInputError('Such product not found');
    }
    return product;
  },

  productsPopulate(products) {
    return products.populate('category').populate('subcategory');
  },

  productPopulate(product) {
    return product.populate('category').populate('subcategory');
  },
};

export default productMixin;
