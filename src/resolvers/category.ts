import {
  Resolver, Query, Arg, Mutation, Authorized,
} from 'type-graphql';
import { ForbiddenError } from 'apollo-server-express';

import { CategoryClass } from '@types';

import { BaseResolver } from '@resolvers';
import { CategoryModel } from '@models';

@Resolver()
export default class CategoryResolver extends BaseResolver {
  @Authorized('admin')
  @Mutation(() => CategoryClass, { description: 'Create a category' })
  async createCategory(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('title') title: string,
    @Arg('icon', { nullable: true }) icon?: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<CategoryClass> {
    const slug = await this.getCategorySlug(title);
    return CategoryModel.create({
      title,
      slug,
      icon,
      subcategories: [],
      products: [],
    });
  }

  @Query(() => [CategoryClass], { description: 'Get all the categories' })
  async categories(): Promise<CategoryClass[]> {
    return this.categoriesPopulate(CategoryModel.find());
  }

  @Query(() => CategoryClass, { description: 'Get the category by id' })
  async category(
    @Arg('id') id: string,
  ): Promise<CategoryClass | null> {
    return this.getPopulatedCategory(id);
  }

  @Authorized('admin')
  @Mutation(() => CategoryClass, { description: 'Update the category' })
  async updateCategory(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('id') id: string,
    @Arg('title', { nullable: true }) title?: string,
    @Arg('icon', { nullable: true }) icon?: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<CategoryClass | null> {
    const category = await this.getCategory(id);

    const update: {
      title?: string,
      slug?: string,
      icon?: string,
    } = {};

    if (title) {
      update.title = title;
      update.slug = await this.getCategorySlug(title);
    }
    update.icon = icon;

    await category.updateOne(update);

    return this.getPopulatedCategory(id);
  }

  @Authorized('admin')
  @Mutation(() => CategoryClass, { description: 'Delete the category' })
  async deleteCategory(
    @Arg('id') id: string,
  ): Promise<CategoryClass | undefined> {
    const category = await this.getCategory(id);
    if (category.subcategories.length) {
      throw new ForbiddenError(
        'This category has subcategories. Please, delete subcategories first',
      );
    }
    if (category.products.length) {
      throw new ForbiddenError(
        'This category has products. Please, delete products first',
      );
    }
    return category.deleteOne();
  }
}
