import {
  Resolver, Arg, Mutation, Query, Authorized,
} from 'type-graphql';
import { ForbiddenError } from 'apollo-server-express';
import { ObjectID } from 'mongodb';

import { SubcategoryClass } from '@types';

import { BaseResolver } from '@resolvers';
import { SubcategoryModel } from '@models';

@Resolver()
export default class SubcategoryResolver extends BaseResolver {
  @Authorized('admin')
  @Mutation(() => SubcategoryClass, { description: 'Create a subcategory' })
  async createSubcategory(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('title') title: string,
    @Arg('categoryId') categoryId: string,
    @Arg('icon', { nullable: true }) icon?: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<SubcategoryClass | null> {
    const category = await this.getCategory(categoryId);
    const slug = await this.getSubcategorySlug(title);
    const subcategory = await SubcategoryModel.create({
      title,
      slug,
      icon,
      category: categoryId,
      products: [],
    });
    category.subcategories.push(subcategory.id);
    await category.save();
    return this.getPopulatedSubcategory(subcategory.id);
  }

  @Query(() => SubcategoryClass, { description: 'Get the subcategory by id' })
  async subcategory(
    @Arg('id') id: string,
  ): Promise<SubcategoryClass | undefined> {
    return this.getPopulatedSubcategory(id);
  }

  @Authorized('admin')
  @Mutation(() => SubcategoryClass, { description: 'Update the subcategory' })
  async updateSubcategory(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('id') id: string,
    @Arg('title', { nullable: true }) title?: string,
    @Arg('icon', { nullable: true }) icon?: string,
    /* eslint-enable @typescript-eslint/indent */

  ): Promise<SubcategoryClass | undefined> {
    const subcategory = await this.getSubcategory(id);

    const update: {
      title: string,
      slug?: string,
      icon?: string,
    } = {
      title: '',
    };

    if (title) {
      update.title = title;
      update.slug = await this.getSubcategorySlug(title);
    }
    update.icon = icon;

    await subcategory.updateOne(update);

    return this.getPopulatedSubcategory(id);
  }

  @Authorized('admin')
  @Mutation(() => SubcategoryClass, { description: 'Delete the subcategory' })
  async deleteSubcategory(
    @Arg('id') id: string,
  ): Promise<SubcategoryClass | undefined> {
    const subcategory = await this.getSubcategory(id);
    if (subcategory.products.length) {
      throw new ForbiddenError(
        'This subcategory has products. Please, delete products first',
      );
    }

    const oldCategory = await this.getCategory(subcategory.category as ObjectID);
    oldCategory.subcategories = oldCategory.subcategories.filter(
      (subcategoryId) => subcategoryId?.toString() !== subcategory.id,
    );
    await oldCategory.save();

    return subcategory.deleteOne();
  }
}
