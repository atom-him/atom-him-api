import {
  Resolver, Query, Arg, Mutation, Authorized,
} from 'type-graphql';
import { UserInputError } from 'apollo-server-express';
import { Ref, DocumentType } from '@typegoose/typegoose';
import { ObjectID } from 'mongodb';

import { SubcategoryClass, ProductClass, ProductInput } from '@types';

import { BaseResolver } from '@resolvers';
import { ProductModel } from '@models';

@Resolver()
export default class ProductResolver extends BaseResolver {
  @Authorized('admin')
  @Mutation(() => ProductClass, { description: 'Create a product' })
  async createProduct(
    @Arg('newProduct') newProduct: ProductInput,
  ): Promise<ProductClass | null> {
    const category = await this.getCategory(newProduct.category);
    let subcategory: DocumentType<SubcategoryClass> | undefined;
    if (newProduct.subcategory) {
      subcategory = await this.getSubcategory(newProduct.subcategory);
      if (subcategory.category?.toString() !== category.id) {
        throw new UserInputError('Such subcategory not found in the category');
      }
    }
    const slug = await this.getProductSlug(newProduct.title);

    const product = await ProductModel.create({
      slug,
      ...newProduct,
    });

    if (subcategory) {
      subcategory.products.push(product.id);
      await subcategory.save();
    }

    category.products.push(product.id);
    await category.save();

    return this.getPopulatedProduct(product.id);
  }

  @Query(() => [ProductClass], { description: 'Get all the products' })
  async products(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('categoryId', { nullable: true }) categoryId?: string,
    @Arg('subcategoryId', { nullable: true }) subcategoryId?: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<Ref<ProductClass>[] | ProductClass[]> {
    if (categoryId && subcategoryId) {
      throw new UserInputError('Please enter a categoryId or a subcategoryId, not both');
    }
    if (categoryId) {
      const category = await this.getPopulatedCategory(categoryId);
      return category.products;
    }
    if (subcategoryId) {
      const subcategory = await this.getPopulatedSubcategory(subcategoryId);
      return subcategory.products;
    }

    return this.productsPopulate(ProductModel.find());
  }

  @Query(() => ProductClass, { description: 'Get product by id' })
  async product(
    @Arg('id') id: string,
  ): Promise<ProductClass | null> {
    return this.getPopulatedProduct(id);
  }

  @Authorized('admin')
  @Mutation(() => ProductClass, { description: 'Update the category' })
  async updateProduct(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('id') id: string,
    @Arg('update') update: ProductInput,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<ProductClass | null> {
    const newCategory = await this.getCategory(update.category);

    let newSubcategory: DocumentType<SubcategoryClass> | undefined;
    if (update.subcategory) {
      newSubcategory = await this.getSubcategory(update.subcategory);
    }

    const product = await this.getProduct(id);

    let newSlug: string | undefined;
    if (update.title !== product.title) {
      newSlug = await this.getProductSlug(update.title);
    }

    const newSubcategoryError = newCategory.subcategories.find((subcategory) => {
      if (subcategory) {
        const subcategoryID = subcategory.toString();
        return newSubcategory && newSubcategory.id !== subcategoryID;
      }
      return false;
    });

    if (newSubcategoryError) {
      throw new UserInputError('Such subcategory not found in the category');
    }

    if (newCategory.id !== product.category?.toString()) {
      const oldSubcategoryError = newCategory.subcategories.find((subcategory) => {
        if (subcategory) {
          const subcategoryID = subcategory.toString();
          return !newSubcategory && subcategoryID !== product.subcategory?.toString();
        }
        return false;
      });

      if (oldSubcategoryError) {
        throw new UserInputError(
          'Product subcategory doesn\'t according with new category',
        );
      }

      const oldCategory = await this.getCategory(product.category as ObjectID);

      oldCategory.products = oldCategory.products.filter(
        (productId) => productId?.toString() !== product.id,
      );
      await oldCategory.save();

      newCategory.products.push(product.id);
      await newCategory.save();
    }

    if (newSubcategory && newSubcategory.id !== product.subcategory?.toString()) {
      const oldSubcategory = await this.getSubcategory(product.subcategory as ObjectID);
      oldSubcategory.products = oldSubcategory.products.filter(
        (productId) => productId?.toString() !== product.id,
      );
      await oldSubcategory.save();

      newSubcategory.products.push(product.id);
      await newSubcategory.save();
    }

    await product.updateOne({ ...update, slug: newSlug });

    return this.getPopulatedProduct(product.id);
  }

  @Authorized('admin')
  @Mutation(() => ProductClass, { description: 'Delete the product' })
  async deleteProduct(@Arg('id') id: string): Promise<ProductClass | undefined> {
    const product = await this.getProduct(id);

    const oldCategory = await this.getCategory(product.category as ObjectID);
    oldCategory.products = oldCategory.products.filter(
      (productId) => productId?.toString() !== product.id,
    );
    await oldCategory.save();

    const oldSubcategory = await this.getSubcategory(product.subcategory as ObjectID);
    oldSubcategory.products = oldSubcategory.products.filter(
      (productId) => productId?.toString() !== product.id,
    );
    await oldSubcategory.save();

    return product.deleteOne();
  }
}
