import {
  GetSlug, GetEntity, EntitiesPopulate, EntityPopulate,
  CategoryClass, ProductClass, SubcategoryClass, CategoryMixin,
  SubcategoryMixin, ProductMixin,
} from '@types';

import { categoryMixin, subcategoryMixin, productMixin } from '@mixins';

class BaseResolver implements CategoryMixin, SubcategoryMixin, ProductMixin {
  getProductSlug: GetSlug;

  getProduct: GetEntity<ProductClass>;

  getPopulatedProduct: GetEntity<ProductClass>;

  productsPopulate: EntitiesPopulate<ProductClass>;

  productPopulate: EntityPopulate<ProductClass>;

  getSubcategorySlug: GetSlug;

  getSubcategory: GetEntity<SubcategoryClass>;

  getPopulatedSubcategory: GetEntity<SubcategoryClass>;

  subcategoryPopulate: EntityPopulate<SubcategoryClass>;

  getCategorySlug: GetSlug;

  getCategory: GetEntity<CategoryClass>;

  getPopulatedCategory: GetEntity<CategoryClass>;

  categoriesPopulate: EntitiesPopulate<CategoryClass>;

  categoryPopulate: EntityPopulate<CategoryClass>;
}

Object.assign(
  BaseResolver.prototype,
  categoryMixin,
  subcategoryMixin,
  productMixin,
);

export default BaseResolver;
