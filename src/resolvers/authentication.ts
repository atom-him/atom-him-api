/* eslint-disable class-methods-use-this */

import {
  Resolver, Arg, Mutation,
} from 'type-graphql';
import { compare, hash } from 'bcryptjs';

import { UserModel, AuthPayloadModel } from '@models';
import { UserInputError } from 'apollo-server-express';
import { sign } from 'jsonwebtoken';
import { APP_SECRET } from '@helpers';
import { AuthPayloadClass } from '@types';

@Resolver()
export default class AuthenticationResolver {
  @Mutation(() => AuthPayloadClass)
  async signIn(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('email') email: string,
    @Arg('password') password: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<AuthPayloadClass> {
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw new UserInputError('No such user found');
    }

    const valid = await compare(password, user.password);
    if (!valid) {
      throw new UserInputError('Invalid password');
    }

    const token = sign({ userId: user.id }, APP_SECRET, { expiresIn: '1h' });

    const authPayload = new AuthPayloadModel({ token, user });

    return authPayload;
  }

  @Mutation(() => AuthPayloadClass)
  async signUp(
    /* eslint-disable @typescript-eslint/indent */
    @Arg('email') email: string,
    @Arg('password') incomingPassword: string,
    /* eslint-enable @typescript-eslint/indent */
  ): Promise<AuthPayloadClass> {
    const password = await hash(incomingPassword, 10);

    const user = await UserModel.create({ email, password, role: 'user' });

    const token = sign({ userId: user.id }, APP_SECRET, { expiresIn: '1h' });

    const authPayload = new AuthPayloadModel({ token, user });

    return authPayload.populate('user');
  }
}
