import 'reflect-metadata';
import express from 'express';
import mongoose from 'mongoose';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import 'module-alias/register';

import {
  CategoryResolver, SubcategoryResolver, ProductResolver, AuthenticationResolver,
} from '@resolvers';
import { authChecker } from '@helpers';

const app: express.Application = express();
const path = '/shop/graphql';
const PORT = process.env.PORT || 4000;
const main = async () => {
  mongoose.connect(
    'mongodb+srv://atom-him:BmIWs9mzjqtAxW5h@m0-sandbox.uc9m2.gcp.mongodb.net/m0-sandbox?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    },
  );
  const schema = await buildSchema({
    resolvers: [
      CategoryResolver,
      SubcategoryResolver,
      ProductResolver,
      AuthenticationResolver,
    ],
    authChecker,
  });
  const apolloServer = new ApolloServer({
    schema,
    introspection: true,
    playground: true,
    tracing: true,
    context: (req) => ({ ...req }),
  });
  apolloServer.applyMiddleware({ app, path });

  app.listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`🚀 started http://localhost:${PORT}${path}`);
  });
};

main();
