import { getModelForClass } from '@typegoose/typegoose';

import { UserClass } from '@types';

export default getModelForClass(UserClass);
