import { getModelForClass } from '@typegoose/typegoose';

import { SubcategoryClass } from '@types';

export default getModelForClass(SubcategoryClass);
