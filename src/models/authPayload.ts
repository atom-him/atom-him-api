import { getModelForClass } from '@typegoose/typegoose';

import { AuthPayloadClass } from '@types';

export default getModelForClass(AuthPayloadClass);
