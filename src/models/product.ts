import { getModelForClass } from '@typegoose/typegoose';

import { ProductClass } from '@types';

export default getModelForClass(ProductClass);
