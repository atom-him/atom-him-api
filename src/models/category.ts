import { getModelForClass } from '@typegoose/typegoose';

import { CategoryClass } from '@types';

export default getModelForClass(CategoryClass);
