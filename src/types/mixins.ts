import {
  GetSlug, GetEntity, EntitiesPopulate, EntityPopulate,
  CategoryClass, SubcategoryClass, ProductClass,
} from '@types';

export interface CategoryMixin {
  readonly getCategorySlug: GetSlug
  readonly getCategory: GetEntity<CategoryClass>
  readonly getPopulatedCategory: GetEntity<CategoryClass>
  readonly categoriesPopulate: EntitiesPopulate<CategoryClass>
  readonly categoryPopulate: EntityPopulate<CategoryClass>
}

export interface SubcategoryMixin {
  readonly getSubcategorySlug: GetSlug
  readonly getSubcategory: GetEntity<SubcategoryClass>
  readonly getPopulatedSubcategory: GetEntity<SubcategoryClass>
  readonly subcategoryPopulate: EntityPopulate<SubcategoryClass>
}

export interface ProductMixin {
  readonly getProductSlug: GetSlug
  readonly getProduct: GetEntity<ProductClass>
  readonly getPopulatedProduct: GetEntity<ProductClass>
  readonly productsPopulate: EntitiesPopulate<ProductClass>
  readonly productPopulate: EntityPopulate<ProductClass>
}
