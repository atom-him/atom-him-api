import { DocumentType } from '@typegoose/typegoose';
import { DocumentQuery } from 'mongoose';
import { ObjectId } from 'mongodb';

export type Entities<T> =
  DocumentQuery<DocumentType<T>[], DocumentType<T>, Record<string, unknown>>;
export type Entity<T> =
  DocumentQuery<DocumentType<T> | null, DocumentType<T>, Record<string, unknown>>;

export type GetSlug = (title: string) => Promise<string>;
export type GetEntity<T> = (id: string | ObjectId) => Promise<DocumentType<T>>;
export type EntitiesPopulate<T> = (products: Entities<T>) => Entities<T>;
export type EntityPopulate<T> = (product: Entity<T>) => Entity<T>;

export type Roles = 'admin' | 'user';
