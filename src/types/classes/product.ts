import {
  ObjectType, Field, ID,
} from 'type-graphql';
import { prop, Ref } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';

import { CategoryClass, SubcategoryClass } from '@types';

@ObjectType()
export default class ProductClass {
  @Field(() => ID)
  id: ObjectId;

  @Field()
  @prop()
  slug: string;

  @Field()
  @prop()
  title: string;

  @Field(() => CategoryClass)
  @prop({ ref: 'CategoryClass' })
  category: Ref<CategoryClass>;

  @Field(() => SubcategoryClass, { nullable: true })
  @prop({ ref: 'SubcategoryClass' })
  subcategory?: Ref<SubcategoryClass>;

  @Field()
  @prop()
  unit: string;

  @Field({ nullable: true })
  @prop()
  image?: string;

  @Field(() => [String], { nullable: true })
  @prop()
  gallery?: string[];

  @Field()
  @prop()
  description: string;

  @Field()
  @prop()
  price: number;

  @Field({ nullable: true })
  @prop()
  salePrice?: number;

  @Field({ nullable: true })
  @prop()
  discountInPercent?: number;
}
