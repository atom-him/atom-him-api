import { ObjectType, Field, ID } from 'type-graphql';
import { prop, Ref } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';

import { SubcategoryClass, ProductClass } from '@types';

@ObjectType()
export default class CategoryClass {
  @Field(() => ID)
  id: ObjectId;

  @Field()
  @prop()
  title: string;

  @Field()
  @prop()
  slug: string;

  @Field({ nullable: true })
  @prop()
  icon?: string;

  @Field(() => [SubcategoryClass])
  @prop({ ref: 'SubcategoryClass' })
  subcategories: Ref<SubcategoryClass>[];

  @Field(() => [ProductClass])
  @prop({ ref: 'ProductClass' })
  products: Ref<ProductClass>[];
}
