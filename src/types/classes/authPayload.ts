import { ObjectType, Field } from 'type-graphql';
import { prop, Ref } from '@typegoose/typegoose';

import { UserClass } from '@types';

@ObjectType()
export default class AuthPayload {
  @Field()
  @prop()
  token: string;

  @Field(() => UserClass)
  @prop({ ref: 'UserClass' })
  user: Ref<UserClass>;
}
