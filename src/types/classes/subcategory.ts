import { ObjectType, Field, ID } from 'type-graphql';
import { prop, Ref } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';

import { ProductClass, CategoryClass } from '@types';

@ObjectType()
export default class SubcategoryClass {
  @Field(() => ID)
  id: ObjectId;

  @Field()
  @prop()
  title: string;

  @Field()
  @prop()
  slug: string;

  @Field(() => CategoryClass)
  @prop({ ref: 'CategoryClass' })
  category: Ref<CategoryClass>;

  @Field({ nullable: true })
  @prop()
  icon?: string;

  @Field(() => [ProductClass])
  @prop({ ref: 'ProductClass' })
  products: Ref<ProductClass>[];
}
