import { ObjectType, Field, ID } from 'type-graphql';
import { prop } from '@typegoose/typegoose';
import { ObjectId } from 'mongodb';

@ObjectType()
export default class UserClass {
  @Field(() => ID)
  id: ObjectId;

  @Field()
  @prop({ unique: true })
  email: string;

  @prop()
  password: string;

  @prop()
  role: string;
}
