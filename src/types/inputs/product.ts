import {
  Field, ID, InputType,
} from 'type-graphql';
import { ObjectId } from 'mongodb';

@InputType()
export default class ProductInput {
  @Field()
  title: string;

  @Field(() => ID)
  category: ObjectId;

  @Field(() => ID, { nullable: true })
  subcategory?: ObjectId;

  @Field()
  unit: string;

  @Field({ nullable: true })
  image?: string;

  @Field(() => [String], { nullable: true })
  gallery?: string[];

  @Field()
  description: string;

  @Field()
  price: number;

  @Field({ nullable: true })
  salePrice?: number;

  @Field({ nullable: true })
  discountInPercent?: number;
}
